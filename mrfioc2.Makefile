#
#  Copyright (c) 2017 - Present  Jeong Han Lee
#  Copyright (c) 2017 - Present  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# Author  : Jeong Han Lee
# email   : han.lee@esss.se
# Date    : Tuesday, March 12 13:41:50 CET 2019
# version : 0.0.7
#

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include ${E3_REQUIRE_TOOLS}/driver.makefile

ifneq ($(strip $(DEVLIB2_DEP_VERSION)),)
devlib2_VERSION=$(DEVLIB2_DEP_VERSION)
endif

# Duplicated one with 3.0.1
# mrfioc2/configure/CONFIG_SITE
USR_CPPFLAGS += -DUSE_TYPED_RSET

# mrfCommon/Makefile 
USR_CFLAGS   += -DDEBUG_PRINT
USR_CPPFLAGS += -DDEBUG_PRINT


# COMMUNITY Dependency 
# mrfCommon (mrfioc2), mrmShared (mrfioc2), epicsvme (devlib2), epicspci (devlib2)
EVGMRMAPP:= evgMrmApp
EVGMRMAPPSRC:=$(EVGMRMAPP)/src
EVGMRMAPPDB:=$(EVGMRMAPP)/Db

SOURCES += $(EVGMRMAPPSRC)/evgInit.cpp

SOURCES += $(EVGMRMAPPSRC)/evg.cpp

SOURCES += $(EVGMRMAPPSRC)/evgMrm.cpp

SOURCES += $(EVGMRMAPPSRC)/evgAcTrig.cpp

SOURCES += $(EVGMRMAPPSRC)/evgEvtClk.cpp

SOURCES += $(EVGMRMAPPSRC)/evgTrigEvt.cpp
SOURCES += $(EVGMRMAPPSRC)/devSupport/devEvgTrigEvt.cpp

SOURCES += $(EVGMRMAPPSRC)/evgMxc.cpp

SOURCES += $(EVGMRMAPPSRC)/evgDbus.cpp
SOURCES += $(EVGMRMAPPSRC)/devSupport/devEvgDbus.cpp

SOURCES += $(EVGMRMAPPSRC)/evgInput.cpp

SOURCES += $(EVGMRMAPPSRC)/evgOutput.cpp

SOURCES += $(EVGMRMAPPSRC)/fct.cpp
SOURCES += $(EVGMRMAPPSRC)/mrmevgseq.cpp

SOURCES += $(EVGMRMAPPSRC)/seqconst.c
SOURCES += $(EVGMRMAPPSRC)/seqnsls2.c

DBDS    += $(EVGMRMAPPSRC)/evgInit.dbd

HEADERS += $(EVGMRMAPPSRC)/evgMrm.h
HEADERS += $(EVGMRMAPPSRC)/evgRegMap.h
HEADERS += $(EVGMRMAPPSRC)/evgAcTrig.h
HEADERS += $(EVGMRMAPPSRC)/evgEvtClk.h
HEADERS += $(EVGMRMAPPSRC)/evgTrigEvt.h
HEADERS += $(EVGMRMAPPSRC)/evgMxc.h
HEADERS += $(EVGMRMAPPSRC)/evgDbus.h
HEADERS += $(EVGMRMAPPSRC)/evgInput.h
HEADERS += $(EVGMRMAPPSRC)/evgOutput.h
HEADERS += $(EVGMRMAPPSRC)/mrmevgseq.h
HEADERS += $(EVGMRMAPPSRC)/fct.h


# COMMUNITY Dependency
# mrfCommon (mrfioc2), mrmShared (mrfioc2), evr (mrfioc2), epicsvme (devlib2), epicspci (devlib2)
EVRMRMAPP:= evrMrmApp
EVRMRMAPPSRC:=$(EVRMRMAPP)/src
EVRMRMAPPDB:=$(EVRMRMAPP)/Db


SOURCES += $(EVRMRMAPPSRC)/drvemIocsh.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemSetup.cpp
SOURCES += $(EVRMRMAPPSRC)/drvem.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemOutput.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemInput.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemPrescaler.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemPulser.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemCML.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemTSBuffer.cpp
SOURCES += $(EVRMRMAPPSRC)/delayModule.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemRxBuf.cpp
SOURCES += $(EVRMRMAPPSRC)/devMrmBuf.cpp

SOURCES += $(EVRMRMAPPSRC)/mrmevrseq.cpp

SOURCES += $(EVRMRMAPPSRC)/bufrxmgr.cpp
SOURCES += $(EVRMRMAPPSRC)/devMrmBufRx.cpp

SOURCES += $(EVRMRMAPPSRC)/os/default/irqHack.cpp

SOURCES += $(EVRMRMAPPSRC)/mrmGpio.cpp

DBDS    += $(EVRMRMAPPSRC)/drvemSupport.dbd





# # COMMUNITY Dependency

# mrfCommon
EVRAPP:= evrApp
EVRAPPSRC:=${EVRAPP}/src
EVRAPPDB:=${EVRAPP}/Db

HEADERS += $(EVRAPPSRC)/evr/pulser.h
HEADERS += $(EVRAPPSRC)/evr/output.h
HEADERS += $(EVRAPPSRC)/evr/delay.h
HEADERS += $(EVRAPPSRC)/evr/input.h
HEADERS += $(EVRAPPSRC)/evr/prescaler.h
HEADERS += $(EVRAPPSRC)/evr/evr.h
HEADERS += $(EVRAPPSRC)/evr/cml.h



DBDS    += $(EVRAPPSRC)/evrSupport.dbd


SOURCES += $(EVRAPPSRC)/evr.cpp

HEADERS += $(EVRAPPSRC)/evrGTIF.h
SOURCES += $(EVRAPPSRC)/evrGTIF.cpp

SOURCES += $(EVRAPPSRC)/devEvrStringIO.cpp

SOURCES += $(EVRAPPSRC)/devEvrEvent.cpp
SOURCES += $(EVRAPPSRC)/devEvrMapping.cpp

SOURCES += $(EVRAPPSRC)/devEvrPulserMapping.cpp

SOURCES += $(EVRAPPSRC)/asub.c
SOURCES += $(EVRAPPSRC)/devWfMailbox.c


SOURCES_Linux += $(EVRAPPSRC)/ntpShm.cpp
SOURCES_DEFAULT += $(EVRAPPSRC)/ntpShmNull.cpp



# COMMUNITY Dependency
# mrfCommon
MRMSHARED:= mrmShared
MRMSHAREDSRC:=${MRMSHARED}/src
MRMSHAREDDB:=${MRMSHARED}/Db


HEADERS += $(MRMSHAREDSRC)/mrmDataBufTx.h
HEADERS += $(MRMSHAREDSRC)/mrmSeq.h
HEADERS += $(MRMSHAREDSRC)/mrmpci.h
HEADERS += $(MRMSHAREDSRC)/sfp.h


SOURCES += $(MRMSHAREDSRC)/mrmDataBufTx.cpp
SOURCES += $(MRMSHAREDSRC)/mrmSeq.cpp
SOURCES += $(MRMSHAREDSRC)/devMrfBufTx.cpp
SOURCES += $(MRMSHAREDSRC)/sfp.cpp
SOURCES += $(MRMSHAREDSRC)/mrmtimesrc.cpp

# After 2.2.0, 2.2.0-ess-rc3,
# mrfioc2.Makefile needs to handel new file with the compatibility with
# old version up to 2.2.0-ess-rc2
SOURCES += $(MRMSHAREDSRC)/mrmspi.cpp

DBDS    += $(MRMSHAREDSRC)/mrmShared.dbd




MRFCOMMON:= mrfCommon/src

HEADERS += $(MRFCOMMON)/mrfBitOps.h
HEADERS += $(MRFCOMMON)/mrfCommon.h

HEADERS += $(MRFCOMMON)/mrfCommonIO.h
HEADERS += $(MRFCOMMON)/mrfFracSynth.h

HEADERS += $(MRFCOMMON)/linkoptions.h
HEADERS += $(MRFCOMMON)/mrfcsr.h

HEADERS += $(MRFCOMMON)/mrf/databuf.h
HEADERS += $(MRFCOMMON)/mrf/object.h

HEADERS += $(MRFCOMMON)/mrf/version.h

HEADERS += $(MRFCOMMON)/devObj.h

HEADERS += $(MRFCOMMON)/configurationInfo.h
HEADERS += $(MRFCOMMON)/plx9030.h
HEADERS += $(MRFCOMMON)/plx9056.h
HEADERS += $(MRFCOMMON)/latticeEC30.h


DBDS    += $(MRFCOMMON)/mrfCommon.dbd

SOURCES += $(MRFCOMMON)/mrfFracSynth.c
SOURCES += $(MRFCOMMON)/linkoptions.c
SOURCES += $(MRFCOMMON)/object.cpp
SOURCES += $(MRFCOMMON)/devObj.cpp
SOURCES += $(MRFCOMMON)/devObjAnalog.cpp
SOURCES += $(MRFCOMMON)/devObjLong.cpp
SOURCES += $(MRFCOMMON)/devObjBinary.cpp
SOURCES += $(MRFCOMMON)/devObjMBB.cpp
SOURCES += $(MRFCOMMON)/devObjMBBDirect.cpp
SOURCES += $(MRFCOMMON)/devObjString.cpp
SOURCES += $(MRFCOMMON)/devObjCommand.cpp
SOURCES += $(MRFCOMMON)/devObjWf.cpp
SOURCES += $(MRFCOMMON)/devMbboDirectSoft.c
SOURCES += $(MRFCOMMON)/devlutstring.cpp
SOURCES += $(MRFCOMMON)/databuf.cpp
SOURCES += $(MRFCOMMON)/mrfCommon.cpp
SOURCES += $(MRFCOMMON)/spi.cpp
SOURCES += $(MRFCOMMON)/flash.cpp
SOURCES += $(MRFCOMMON)/flashiocsh.cpp
SOURCES += $(MRFCOMMON)/pollirq.cpp

# After 2.2.0, 2.2.0-ess-rc3,
# mrfioc2.Makefile needs to handel new file with the compatibility with
# old version up to 2.2.0-ess-rc2
SOURCES += $(MRFCOMMON)/pollirq.cpp

SOURCES += $(MRFCOMMON)/flashiocsh.cpp



HEADERS += $(MRFCOMMON)/mrf/version.h

# This is a bit of a hack, since this is set in ${CONFIG}/CONFIG_COMMON which isn't loaded by the time we build the `prebuild` target.
ifeq ($(strip $(PERL)),)
PERL = perl -CSD
endif

prebuild: version_header_file

.PHONY: version_header_file
version_header_file: 
	$(PERL) -I$(EPICS_BASE)/lib/perl $(where_am_I)/$(MRFCOMMON)/genVersionHeader.pl -t "" -V $(E3_MODULE_VERSION) -N MRF_VERSION $(where_am_I)/$(MRFCOMMON)/mrf/version.h


SCRIPTS += $(wildcard ../iocsh/*.iocsh)

TEMPLATES += $(wildcard $(MRMSHAREDDB)/*.db)
TEMPLATES += $(wildcard $(EVRAPPDB)/*.db)
TEMPLATES += $(wildcard $(EVGMRMAPPDB)/*.db)
TEMPLATES += $(wildcard $(EVRMRMAPPDB)/*.db)

TEMPLATES += $(wildcard $(MRMSHAREDDB)/*.template)
TEMPLATES += $(wildcard $(EVRAPPDB)/*.template)
TEMPLATES += $(wildcard $(EVGMRMAPPDB)/*.template)
TEMPLATES += $(wildcard $(EVRMRMAPPDB)/*.template)

TEMPLATES += $(wildcard ../template/evr-*.template)
TEMPLATES += $(wildcard ../db/evr-*-ess.db)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(MRMSHAREDDB)
USR_DBFLAGS += -I $(EVGMRMAPPDB)
USR_DBFLAGS += -I $(EVRMRMAPPDB)
USR_DBFLAGS += -I $(EVRAPPDB)
USR_DBFLAGS += -I $(where_am_I)/../template

EVG_SUBS = $(wildcard $(EVGMRMAPPDB)/*.substitutions)
EVG_TMPS = $(wildcard $(EVGMRMAPPDB)/*.template)
EVR_SUBS = $(wildcard $(EVRMRMAPPDB)/*.substitutions)

ESS_SUBS = $(wildcard ../template/evr-*.substitutions)
TEMPLATES += $(wildcard ../template/evr-*.db)

.PHONY: db
db:

.PHONY: prebuild
prebuild: $(EVG_SUBS) $(EVR_SUBS) $(EVG_TMPS) $(ESS_SUBS) db_fix_pv_naming_convention

.PHONY: $(EVG_SUBS) $(EVR_SUBS) $(ESS_SUBS)
$(EVG_SUBS) $(EVR_SUBS) $(ESS_SUBS):
	@printf "Inflating database ... %48s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db -S $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db -S $@

.PHONY: $(EVG_TMPS)
$(EVG_TMPS):
	@printf "Inflating database ... %48s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db $@


define MACRO_SUBSTITUTION
  /\$$\((SYS|D)\)/{ \
    s/\{\$$\(D\)\}?/$$(D)/g; \
    s/\$$\(D\)-/$$(D)/g; \
    s/\}/-/g; \
    s/:/-/g; \
    s/\$$\(SYS\)\$$\(D\)/$$(P)$$(R=)$$(S=:)/g \
  }
endef

MACRO_SUBSTITUTION_FILES += $(EVGMRMAPPDB)/mtca-evm-300.db
MACRO_SUBSTITUTION_FILES += $(EVGMRMAPPDB)/cpci-evg-300.db
MACRO_SUBSTITUTION_FILES += $(EVRMRMAPPDB)/evr-mtca-300.db
MACRO_SUBSTITUTION_FILES += $(EVRMRMAPPDB)/evr-pcie-300dc.db

.PHONY: db_fix_pv_naming_convention
db_fix_pv_naming_convention: $(MACRO_SUBSTITUTION_FILES)
	for f in $^; do \
	  sed -E '$(MACRO_SUBSTITUTION)' $$f > $${f%.*}-univ.db; \
	done

#
.PHONY: vlibs
vlibs:
#
