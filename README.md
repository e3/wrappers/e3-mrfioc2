# e3-mrfioc2

Wrapper for the module `mrfioc2`.

For a project history, see the [CHANGELOG.md](CHANGELOG.md).

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh.bash -r mrfioc2
```

## Contributing

Contributions through pull/merge requests only.
