# .cmd
require "mrfioc2" "2.2.1rc1"

epicsEnvSet "EPICS_CA_MAX_ARRAY_BYTES" "10000000"

#epicsEnvSet "TOP" "$(E3_IOCSH_TOP)/.."

## Load record instances
iocshLoad "$(mrfioc2_DIR)/evm.iocsh" "P=$(PEVM), PCIID=$(PCI_SLOT)"

iocInit

# EOF
