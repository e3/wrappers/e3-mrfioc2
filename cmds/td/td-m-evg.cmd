﻿# .cmd
require "mrfioc2" "2.2.1rc1"
#require "supercycleengine"

#epicsEnvSet "ENGINEER" "ICS_HWI_WP04"
epicsEnvSet "EPICS_CA_MAX_ARRAY_BYTES" "10000000"

# Define the reftabs location
#epicsEnvSet "DBUFL"         "$(reftabs_DIR)/init/databuffer-ess.json"
#epicsEnvSet "MEVTSL"        "$(reftabs_DIR)/init/mevts-ess.json"
#epicsEnvSet "SCTROOT"       "$(reftabs_DIR)/supercycles/"

# Load common environment
#iocshLoad "$(E3_COMMON_DIR)/e3-common.iocsh"

epicsEnvSet "TOP"  "$(E3_IOCSH_TOP)/../.."
epicsEnvSet "PEVG" "TD-M:Ctrl-EVG-1"

iocshLoad "$(TOP)/iocsh/mtca.iocsh"

# Load record instances
iocshLoad "$(mrfioc2_DIR)/evm.iocsh"                "P=$(PEVG), OBJ=EVG,   PCIID=$(MTCA_9U_PCIID2)"
#iocshLoad "$(supercycleengine_DIR)/sce.iocsh"       "P=$(PSCE), PG=$(PEVG)"

iocInit

iocshLoad "$(mrfioc2_DIR)/evgr.iocsh"               "P=$(PEVG), INTRF=, INTPPS="
#iocshLoad "$(mrfioc2_DIR)/evgasynr.iocsh"           "P=$(PEVG), $(ASYNARGS=)"
iocshLoad "$(TOP)/iocsh/evgasynr.iocsh"             "P=$(PEVG), EVT_PMORTEM=$(EVT_PMORTEM=40), EVT_PMORTEMI=$(EVT_PMORTEMI=41), EVT_DOD=$(EVT_DOD=42), EVT_DODI=$(EVT_DODI=43)"
#iocshLoad "$(supercycleengine_DIR)/evgseqr.iocsh"   "P=$(PEVG)"

# EOF

