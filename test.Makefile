TOP:=$(CURDIR)
PACKAGE_MANAGER:=command -v yum dnf apt

include $(TOP)/configure/CONFIG
where_am_I:=$(CURDIR)/$(EPICS_MODULE_NAME)

all:
	make init
	make -f test.Makefile prebuild
	make build
	make install

pragma:
	python3 -m macropragma -v
	python3 -m macropragma -c $(where_am_I)/../patch/conf.yaml -d $(where_am_I)/

prepare:
	sudo $(PACKAGE_MANAGER) install python3 python3-pip
	python3 -m pip install --user macropragma==$(PIP_MACROPRAGMA_DEP_VERSION)

prebuild:
	for i in $(where_am_I)/../patch/*.patch; do \
		if ! patch -R -p0 -s -f --dry-run -d $(where_am_I)/../ <$$i &>/dev/null; then \
			patch -p0 -d $(where_am_I)/../ <$$i ; \
		fi \
	done
